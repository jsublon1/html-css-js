var choixPers = 0;
function afficherInfo(radio) {
  let field = document.getElementById("info");

  if (radio.value == "client") {
    field.innerHTML = `<div class="row"><label for="dateNaissance" class="col-5">Date de Naissance</label><input type="date" name="dateNaissance" class="col-7"></div>
        <div class="row"><label for="numSecu" class="col-5">Numéro de Sécurité Sociale</label><input type="text" name="numSecu" class="col-7"></div>
        <div class="row">
            <label for="choix-mutuelle" class="col-5">Votre Mutuelle</label>
            <select name="mutuelle" id="choix-mutuelle" class="col-7">
            <option value="">--Choisissez votre mutuelle</option>
            <option value="abcd">ABCD</option>
            <option value="entubeur">Emtubeur Mutuelle</option>
            <option value="mystere">Mysterios</option>
         </select>
        </div>
        <div class="row"><label for="choix-medecin" class="col-5">Votre Médecin traitant</label>
            <select name="medecin" id="choix-medecin" class="col-7">
                <option value="">--Choisissez votre médecin traitant</option>
                <option value="foldingue">Dr. Foldingue</option>
                <option value="retournay">Dr. Retournay</option>
                <option value="dorian">Dr. Dorian</option>
            </select></div>
        <div class="row">
            <label for="choix-specialiste" class="col-5">Médecin spécialiste</label>
            <select name="specialiste" id="choix-specialiste" class="col-7">
                <option value="">--Choisissez votre médecin spécialiste</option>
                <option value="bones">Dr. Bones</option>
                <option value="persin">Dr. Persin</option>
                <option value="bayat">Dr. Bayat</option>
            </select>
        </div>`;
  }
  if (radio.value == "medecin") {
    field.innerHTML = `<div class="row"><label for="numAgrement" class="col-5">Numéro d'Agrément</label><input type="text" name="numAgrement" class="col-7"></div>`;
  }
  if (radio.value == "specialiste") {
    field.innerHTML = `<div class="row">
        <label for="choix-specialite" class="col-5">Spécialité</label>
        <select name="specialite" id="choix-specialite" class="col-7">
            <option value="">--Choisissez la spécialité</option>
            <option value="cardio">Cardiologue</option>
            <option value="onco">Oncologue</option>
            <option value="dermato">Dermatologue</option>
        </select>
    </div>`;
  }
}
function verifier() {
  let box = document.getElementsByName("personne");
  let verif = false;

  for (let elem of box) {
    if (elem.checked) verif = true;
  }
  alert(verif);
  if (verif === false) alert("Aucun type de personne sélectionné");

  return verif;
}
